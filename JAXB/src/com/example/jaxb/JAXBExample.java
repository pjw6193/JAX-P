package com.example.jaxb;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.example.domain.Branch;
import com.example.domain.Leaf;
import com.example.domain.Tree;
import com.example.domain.Trunk;

public class JAXBExample {
	
	public static void main(String[] args) {
		
		String file = "src/xml/tree1.xml";
		
		/**
		 * Marshal example
		 */
		Tree tree = JAXBExample.createTree();
		JAXBExample.marshal(tree, file);
		
		/**
		 * Unmarshal example
		 */
		Tree output = JAXBExample.unmarshal(file);
		System.out.println(output);
	}
	
	public static void marshal(Object object, String filePath){
		MarshallFactory factory = new MarshallFactory();
		Marshaller marshaller = factory.getMarshaller(object);
		try {
			marshaller.marshal(object, new File(filePath));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	public static Tree unmarshal(String filePath){
		MarshallFactory factory = new MarshallFactory();
		Unmarshaller unmarshaller = factory.getUnmarshaller(new Tree());
		Tree tree = null;
		try {
			tree = (Tree) unmarshaller.unmarshal(new File(filePath));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return tree;
	}
	
	public static Tree createTree(){
		// Branch 1 with leaves
		Set<Leaf> leaves1 = new HashSet<Leaf>();
		
		Leaf leaf1 = new Leaf("Green");
		leaves1.add(leaf1);
		
		Leaf leaf2 = new Leaf("Red");
		leaves1.add(leaf2);
		
		Branch branch1 = new Branch(leaves1);
		
		// Branch 2 with leaves
		Set<Leaf> leaves2 = new HashSet<Leaf>();
		
		Leaf leaf3 = new Leaf("Yellow");
		leaves2.add(leaf3);
		
		Leaf leaf4 = new Leaf("Brown");
		leaves2.add(leaf4);
		
		Branch branch2 = new Branch(leaves2);
		
		// Add branches to trunk and trunk to tree
		Set<Branch> branches1 = new HashSet<Branch>();
		branches1.add(branch1);
		branches1.add(branch2);
		Trunk trunk1 = new Trunk(10, branches1);
		Tree tree1 = new Tree("maple", trunk1);
		
		return tree1;
	}

}
