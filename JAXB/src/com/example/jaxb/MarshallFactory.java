package com.example.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class MarshallFactory {

	private Marshaller marshaller;
	private Unmarshaller unmarshaller;
	
	public Marshaller getMarshaller(Object object) {
		
		try {
			
			JAXBContext context = JAXBContext.newInstance(object.getClass());
			marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return marshaller;
	}
	public Unmarshaller getUnmarshaller(Object object) {
		try {
			
			JAXBContext context = JAXBContext.newInstance(object.getClass());
			unmarshaller = context.createUnmarshaller();
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return unmarshaller;
	}
	
	
}
