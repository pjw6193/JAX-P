package com.example.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Leaf {

	private String color;

	public Leaf(){
		super();
	}
	public Leaf(String color) {
		super();
		this.color = color;
	}
	public String getColor() {
		return color;
	}
	@XmlAttribute
	public void setColor(String color) {
		this.color = color;
	}
	@Override
	public String toString() {
		return "Leaf [color=" + color + "]";
	}
	
	
}
