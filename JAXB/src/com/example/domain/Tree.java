package com.example.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Tree {
	
	private String type;
	private Trunk trunk;
	
	public Tree(){
		super();
	}
	
	public Tree(String type, Trunk trunk) {
		super();
		this.type = type;
		this.trunk = trunk;
	}
	public String getType() {
		return type;
	}
	@XmlAttribute
	public void setType(String type) {
		this.type = type;
	}
	public Trunk getTrunk() {
		return trunk;
	}
	@XmlElement
	public void setTrunk(Trunk trunk) {
		this.trunk = trunk;
	}
	@Override
	public String toString() {
		return "Tree [type=" + type + ", trunk=" + trunk + "]";
	}
	
	
}
