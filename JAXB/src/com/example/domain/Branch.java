package com.example.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Branch {

	Set<Leaf> leaves;
	
	public Branch(){
		super();
	}
	public Branch(Set<Leaf> leaves) {
		super();
		this.leaves = leaves;
	}
	public Set<Leaf> getLeaves() {
		return leaves;
	}
	@XmlElement
	public void setLeaves(Set<Leaf> leaves) {
		this.leaves = leaves;
	}
	@Override
	public String toString() {
		return "Branch [leaves=" + leaves + "]";
	}
	
	
}
