package com.example.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Trunk {
	
	private int width;
	private Set<Branch> branches;
	
	public Trunk(){
		super();
	}
	
	public Trunk(int width, Set<Branch> branches) {
		super();
		this.width = width;
		this.branches = branches;
	}
	public int getWidth() {
		return width;
	}
	@XmlAttribute
	public void setWidth(int width) {
		this.width = width;
	}
	public Set<Branch> getBranches() {
		return branches;
	}
	@XmlElement
	public void setBranches(Set<Branch> branches) {
		this.branches = branches;
	}
	@Override
	public String toString() {
		return "Trunk [width=" + width + ", branches=" + branches + "]";
	}
	
	
}
