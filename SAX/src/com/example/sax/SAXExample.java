package com.example.sax;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class SAXExample {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		// Parser Configuration
		File file = new File("src/xml/history.xml");
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		HistoricalEventHandler handler = new HistoricalEventHandler();
		parser.parse(file, handler);
		
	}
	
}
