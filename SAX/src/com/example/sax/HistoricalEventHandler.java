package com.example.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class HistoricalEventHandler extends DefaultHandler{
	
	// Events for SAX Parser
	boolean boolEvent = false;
	boolean boolTitle = false;
	boolean boolYear = false;
	String eventNumber = null;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if(qName.equalsIgnoreCase("event")){
			eventNumber = attributes.getValue("num");
			boolEvent = true;
		}else if(qName.equalsIgnoreCase("title")){
			boolTitle = true;
		}else if(qName.equalsIgnoreCase("year")){
			boolYear = true;
		}
		
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
	
		if(qName.equalsIgnoreCase("history")){
			System.out.println("--end of file--");
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		
		if(boolEvent){
			System.out.println(new String(ch, start, length));
			this.restart();
		}else if(boolTitle){
			System.out.println(eventNumber + ": " + new String(ch, start, length));
			this.restart();
		}else if(boolYear){
			System.out.println(new String(ch, start, length));
			this.restart();
		}
	}
	
	private void restart(){
		boolEvent = false;
		boolTitle = false;
		boolYear = false;
	}
}
