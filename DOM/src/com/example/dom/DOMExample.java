package com.example.dom;

import com.example.domain.Branch;
import com.example.domain.Leaf;
import com.example.domain.Tree;
import com.example.domain.Trunk;

public class DOMExample {
	
	public static void main(String[] args) {
		TreeDriver handler = new TreeDriver();
		Tree tree = handler.createTree();
		
		System.out.println("Tree type="+tree.getType());
		
		Trunk trunk = tree.getTrunk();
		System.out.println("Trunk width="+trunk.getWidth());
		
		int i = 1;
		for(Branch branch:trunk.getBranches()){
			System.out.println("Branch " + i);
			for(Leaf leaf:branch.getLeaves()){
				System.out.println(leaf.getColor());
			}
			i++;
		}
		
	}
}