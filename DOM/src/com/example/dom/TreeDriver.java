package com.example.dom;

import java.util.HashSet;
import java.util.Set;

import javax.xml.soap.Node;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.example.domain.Branch;
import com.example.domain.Leaf;
import com.example.domain.Tree;
import com.example.domain.Trunk;

public class TreeDriver {

	private final String XML = "src/xml/tree.xml";
	private Tree tree;
	
	public Tree createTree(){

		Document doc = DocumentManager.getDocument(XML);
		
		// Preinitialize tree
		tree = new Tree();
		tree.setType(doc.getDocumentElement().getAttribute("type"));
		
		// Preinitialize trunk
		Trunk trunk = new Trunk();
		trunk.setWidth(Integer.parseInt(
				doc.getElementsByTagName("trunk").
				item(0).getAttributes().getNamedItem("width").getNodeValue()));
		
		// Initialize branch set
		int numberOfBranches = doc.getElementsByTagName("branch").getLength();
		Set<Branch> branches = new HashSet<Branch>();
		for (int i = 0; i < numberOfBranches; i++) {
			Branch branch = new Branch();
			
			// Initialize leave set
			NodeList leafList = doc.getElementsByTagName("branch").
					item(i).getChildNodes();
			
			Set<Leaf> leaves = new HashSet<Leaf>();
			for (int j = 0; j < leafList.getLength(); j++) {

				if(leafList.item(j).getNodeType()==Node.ELEMENT_NODE){
					Leaf leaf = new Leaf();
					leaf.setColor(leafList.item(j).getTextContent());
					leaves.add(leaf);
				}
				
			}//end leaf loop
			
			branch.setLeaves(leaves);
			branches.add(branch);
		}//end branch loop

		trunk.setBranches(branches);
		tree.setTrunk(trunk);
		
		return tree;
	}
}
