package com.example.dom;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DocumentManager {

	private static DocumentBuilder documentBuilder;
	private static Document document;
	
	private DocumentManager(){
		DocumentBuilderFactory documentBuilderFactory 
				= DocumentBuilderFactory.newInstance();
		try {
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	private static DocumentBuilder getDocumentBuilder(){
		if (documentBuilder == null){
			new DocumentManager();
			return documentBuilder;
		}else
			return documentBuilder;
	}
	
	public static Document getDocument(String path){
		File file = new File(path);
		try {
			document = DocumentManager.getDocumentBuilder().parse(file);
			document.getDocumentElement().normalize();
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		return document;
	}
}
