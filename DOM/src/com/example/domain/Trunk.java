package com.example.domain;

import java.util.Set;

public class Trunk {

	private int width;
	private Set<Branch> branches;
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public Set<Branch> getBranches() {
		return branches;
	}
	public void setBranches(Set<Branch> branches) {
		this.branches = branches;
	}
}
