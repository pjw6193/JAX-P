package com.example.domain;

import java.util.Set;

public class Branch {

	Set<Leaf> leaves;
	
	public Set<Leaf> getLeaves() {
		return leaves;
	}
	public void setLeaves(Set<Leaf> leaves) {
		this.leaves = leaves;
	}
}
